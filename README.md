# Heat control made with QML

With this project I'm trying to fixate what I have learnt of QML.

The idea is to provide as heat control interface for my house's heater system.

It makes use of [GIT flow](https://nvie.com/posts/a-successful-git-branching-model/), so remember:

- master is the production branch.
- develop is the actual development branch from which you should start working
  from.

Happy hacking!
