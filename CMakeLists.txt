cmake_minimum_required(VERSION 3.14)

project(heatcontrol LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check https://doc.qt.io/qt/deployment-android.html for more information.
# They need to be set before the find_package(...) calls below.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core Quick LinguistTools QuickControls2 REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core Quick LinguistTools REQUIRED)

# Versioning ####

# Application version.
set(HEATCONTROL_MAJOR_VERSION "0")
set(HEATCONTROL_MINOR_VERSION "0")
set(HEATCONTROL_PATCH_VERSION "2")
# Release/beta candidate, keep empty on official versions.
set(HEATCONTROL_RC_VERSION "~")

set(HEATCONTROL_VERSION "${HEATCONTROL_MAJOR_VERSION}.${HEATCONTROL_MINOR_VERSION}.${HEATCONTROL_PATCH_VERSION}${HEATCONTROL_RC_VERSION}")
set(HEATCONTROL_VERSION_STRING "${HEATCONTROL_VERSION}")

# If git is installed add the revision to HEATCONTROL_VERSION_STRING.
# This is useful to detect development builds, as final builds will not have
# git available.
if(EXISTS "${monitor_SOURCE_DIR}/.git")
  find_program(GIT_EXECUTABLE NAMES git)
  if(GIT_EXECUTABLE)
    message(STATUS "Found git: ${GIT_EXECUTABLE}")
    execute_process(COMMAND ${GIT_EXECUTABLE} describe
                    WORKING_DIRECTORY ${monitor_SOURCE_DIR}
                    OUTPUT_VARIABLE HEATCONTROL_GIT_DESCRIBE)
    string(REGEX REPLACE "\n" "" HEATCONTROL_GIT_DESCRIBE "${HEATCONTROL_GIT_DESCRIBE}")
    string(REGEX REPLACE "${HEATCONTROL_VERSION}-" "" HEATCONTROL_GIT_DESCRIBE "${HEATCONTROL_GIT_DESCRIBE}")
    set(HEATCONTROL_GIT_DESCRIBE ${HEATCONTROL_GIT_DESCRIBE})
    set(HEATCONTROL_VERSION_STRING "${HEATCONTROL_VERSION_STRING} revision ${HEATCONTROL_GIT_DESCRIBE}")
  endif()
endif()

# Create the definitions.
add_definitions(
    -DHEATCONTROL_MAJOR_VERSION="${HEATCONTROL_MAJOR_VERSION}"
    -DHEATCONTROL_MINOR_VERSION="${HEATCONTROL_MINOR_VERSION}"
    -DHEATCONTROL_PATCH_VERSION="${HEATCONTROL_PATCH_VERSION}"
    -DHEATCONTROL_RC_VERSION="${HEATCONTROL_RC_VERSION}"
    -DHEATCONTROL_GIT_DESCRIBE="${HEATCONTROL_GIT_DESCRIBE}"
    -DHEATCONTROL_VERSION="${HEATCONTROL_VERSION}"
    -DHEATCONTROL_VERSION_STRING="${HEATCONTROL_VERSION_STRING}"
)

# Log the versions.
message(STATUS "heatcontrol version: ${HEATCONTROL_VERSION}")
message(STATUS "Git describe: ${HEATCONTROL_GIT_DESCRIBE}")


add_subdirectory(src)
add_subdirectory(tests)
