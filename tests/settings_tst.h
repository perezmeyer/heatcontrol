#ifndef SETTINGS_TESTS_H
#define SETTINGS_TESTS_H

#include <QObject>

class SettingsTests : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void checkTargetTemp();
    void checkHysteresis();

    void checkProfileDayInSlot();

    void checkTurnedOn();

    void cleanup();
};

#endif // SETTINGS_TESTS_H
