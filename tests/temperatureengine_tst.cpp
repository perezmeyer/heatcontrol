#include <QObject>
#include <QTest>
#include <QDebug>
#include <QTime>
#include <QString>
#include <QSignalSpy>

#include "../src/settings.h"
#include "../src/temperatureengine.h"
#include "../src/abstracttemperaturesensor.h"
#include "../src/faketemperaturesensor.h"

#include "temperatureengine_tst.h"

void TemperatureEngineTests::checkOperationModes()
{
    auto te = TemperatureEngine::instance();

    QVERIFY(te->operationMode() == TemperatureEngine::Normal);

    te->setTimeOverride(1, 160, Settings::TwoDec);
    QVERIFY(te->operationMode() == TemperatureEngine::TimeOverride);

    te->setTemperatureOverride(Settings::ThreeDec);
    QVERIFY(te->operationMode() == TemperatureEngine::TemperatureOverride);

    te->setNormalOperationMode();
    QVERIFY(te->operationMode() == TemperatureEngine::Normal);
}

void TemperatureEngineTests::checkTurnedOn()
{
    auto te = TemperatureEngine::instance();
    te->setNormalOperationMode();
    te->setTurnedOn(false);
    QSignalSpy turnedOnSpy(te, &TemperatureEngine::turnedOnChanged);
    QSignalSpy heaterOnSpy(te, &TemperatureEngine::heaterOnChanged);

    te->setTurnedOn(true);
    QCOMPARE(turnedOnSpy.count(), 1);
    QCOMPARE(heaterOnSpy.count(), 0);
    QVERIFY(te->turnedOn() == true);
    turnedOnSpy.clear();
    heaterOnSpy.clear();

    te->setHeaterOn(true);
    heaterOnSpy.clear();

    te->setTurnedOn(false);
    QCOMPARE(turnedOnSpy.count(), 1);
    QCOMPARE(heaterOnSpy.count(), 1);
    QVERIFY(te->turnedOn() == false);
    QVERIFY(te->heaterOn() == false);
}

void TemperatureEngineTests::checkHeaterOn()
{
    setAllSlotsToFirstProfile();

    auto te = TemperatureEngine::instance();
    te->setNormalOperationMode();
    te->setHeaterOn(false);
    QSignalSpy heaterOnSpy(te, &TemperatureEngine::heaterOnChanged);

    te->setHeaterOn(true);
    QCOMPARE(heaterOnSpy.count(), 1);
    QVERIFY(te->heaterOn() == true);
    heaterOnSpy.clear();

    te->setHeaterOn(false);
    QCOMPARE(heaterOnSpy.count(), 1);
    QVERIFY(te->heaterOn() == false);
}

void TemperatureEngineTests::checkTimeToSlot_data()
{
    QTest::addColumn<QTime>("time");
    QTest::addColumn<int>("slot");

    QTime time(0, 0, 0, 0);
    // 24hs * 60 min * 60 secs / numberOfSlot
    const int DELTA_SECS = 24 *60 * 60 / Settings::numberOfSlots();

    for(int slot = 0; slot < Settings::numberOfSlots(); slot++)
    {
        QTest::addRow("Slot_%d_start", slot) << time << slot;
        QTest::addRow("Slot_%d_end", slot) << time.addSecs(DELTA_SECS - 1) << slot;

        time = time.addSecs(DELTA_SECS);
    }
}

void TemperatureEngineTests::checkTimeToSlot()
{
    QFETCH(QTime, time);
    QFETCH(int, slot);

    QVERIFY(TemperatureEngine::timeToSlot(time) == slot);
}

void TemperatureEngineTests::checkEvaluateStatus()
{
    auto settings = Settings::instance();

    // Set target temperature to 15.0 ºC.
    const int TARGET_TEMP = 150;
    settings->setTargetTempDecDegC(TARGET_TEMP, Settings::FirstProfile);
    settings->setHysteresis(Settings::OneDec, Settings::FirstProfile);

    setAllSlotsToFirstProfile();

    auto te = TemperatureEngine::instance();
    te->setNormalOperationMode();

    FakeTemperatureSensor * fs = dynamic_cast<FakeTemperatureSensor*>(te->mTempSensor);
    QVERIFY(fs != nullptr);

    // Let's turn on the heat.
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP - static_cast<int>(Settings::OneDec));
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == true);

    // Heater must be kept on.
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP);
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == true);

    // Heather must still be kept on
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP + static_cast<int>(Settings::OneDec) - 1);
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == true);

    // Heater must be turned off.
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP + static_cast<int>(Settings::OneDec));
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == false);

    // Heater must be kept off.
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP);
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == false);

    // Heater must still be kept off.
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP - static_cast<int>(Settings::OneDec) + 1);
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == false);

    // Let's turn on the heater again.
    fs->setCurrentTemperatureDecDegC(TARGET_TEMP - static_cast<int>(Settings::OneDec));
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == true);
}

void TemperatureEngineTests::checkTemperatureOverrideOperMode()
{
    auto te = TemperatureEngine::instance();

    FakeTemperatureSensor * fs = dynamic_cast<FakeTemperatureSensor*>(te->mTempSensor);
    QVERIFY(fs != nullptr);

    fs->setCurrentTemperatureDecDegC(150);
    te->setTemperatureOverride(Settings::ThreeDec);
    te->evaluateStatus();
    QVERIFY(te->mOverrideTargetTemp == 153);
    QVERIFY(te->heaterOn() == true);

    fs->setCurrentTemperatureDecDegC(153);
    te->evaluateStatus();
    QVERIFY(te->heaterOn() == false);
}

void TemperatureEngineTests::setAllSlotsToFirstProfile()
{
    auto settings = Settings::instance();

    // Set slots of all days to FirstProfile.
    for(int day = static_cast<int>(Settings::Monday); day <= static_cast<int>(Settings::Sunday); day++)
    {
        for(int slot = 0; slot < settings->numberOfSlots(); slot++)
            settings->setProfileInDaySlot(static_cast<Settings::Day>(day), slot, Settings::FirstProfile);
    }
}

QTEST_MAIN(TemperatureEngineTests)
#include "temperatureengine_tst.moc"
