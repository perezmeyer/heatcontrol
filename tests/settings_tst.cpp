#include <QObject>
#include <QTest>
#include <QDebug>
#include <QSettings>
#include <QFile>

#include "../src/settings.h"

#include "settings_tst.h"

void SettingsTests::initTestCase()
{
    qApp->setApplicationName(QStringLiteral("heatcontroltests"));
    qApp->setOrganizationName(QStringLiteral("DeleteMe"));
}

void SettingsTests::checkTargetTemp()
{
    auto settings = Settings::instance();

    QVERIFY(settings->setTargetTempDecDegC(Settings::minTempDecDegC() - 1, Settings::FirstProfile) == false);

    QVERIFY(settings->setTargetTempDecDegC(Settings::minTempDecDegC(), Settings::FirstProfile) == true);
    QVERIFY(settings->targetTempDecDegC(Settings::FirstProfile) == Settings::minTempDecDegC());

    QVERIFY(settings->setTargetTempDecDegC(Settings::maxTempDecDegC(), Settings::SecondProfile) == true);
    QVERIFY(settings->targetTempDecDegC(Settings::SecondProfile) == Settings::maxTempDecDegC());

    QVERIFY(settings->setTargetTempDecDegC(Settings::maxTempDecDegC() + 1, Settings::ThirdProfile) == false);
}

void SettingsTests::checkHysteresis()
{
    auto settings = Settings::instance();

    settings->setHysteresis(Settings::ThreeDec, Settings::SecondProfile);
    QVERIFY(settings->hysteresis(Settings::SecondProfile) == Settings::ThreeDec);
}

void SettingsTests::checkProfileDayInSlot()
{
    auto settings = Settings::instance();

    QVERIFY(settings->setProfileInDaySlot(Settings::Wednesday, -1, Settings::FirstProfile) == false);
    QVERIFY(settings->setProfileInDaySlot(Settings::Wednesday, Settings::numberOfSlots(), Settings::FirstProfile) == false);
    QVERIFY(settings->setProfileInDaySlot(Settings::Wednesday, Settings::numberOfSlots() / 2, Settings::ThirdProfile) == true);

    QVERIFY(settings->profileInDaySlot(Settings::Wednesday, Settings::numberOfSlots() / 2) == Settings::ThirdProfile);
}

void SettingsTests::checkTurnedOn()
{
    auto settings = Settings::instance();

    settings->setTurnedOn(true);
    QVERIFY(settings->turnedOn() == true);

    settings->setTurnedOn(false);
    QVERIFY(settings->turnedOn() == false);
}

void SettingsTests::cleanup()
{
    QSettings settings;
    QFile file(settings.fileName());

    if(file.exists())
        file.remove();
}

QTEST_MAIN(SettingsTests)
#include "settings_tst.moc"
