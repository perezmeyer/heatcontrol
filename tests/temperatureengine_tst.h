#ifndef TEMPERATUREENGINE_TESTS_H
#define TEMPERATUREENGINE_TESTS_H

#include <QObject>

class TemperatureEngineTests : public QObject
{
    Q_OBJECT

private slots:
    void checkOperationModes();

    void checkTurnedOn();
    void checkHeaterOn();

    void checkTimeToSlot_data();
    void checkTimeToSlot();

    void checkEvaluateStatus();

    void checkTemperatureOverrideOperMode();

    void setAllSlotsToFirstProfile();
};

#endif // TEMPERATUREENGINE_TESTS_H
