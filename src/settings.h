#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QJSEngine>
#include <QVector>

class QSettings;

class Settings : public QObject
{
    Q_OBJECT

public:

    // Follows QDate::dayOfWeek().
    enum Day {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7
    };
    Q_ENUM(Day)

    enum Profile {
        FirstProfile = 0,
        SecondProfile = 1,
        ThirdProfile = 2,
        InvalidProfile
    };
    Q_ENUM(Profile)

    enum Hysteresis {
        OneDec = 1,
        TwoDec = 2,
        ThreeDec = 3,
        FourDec = 4,
        FiveDec = 5,
    };
    Q_ENUM(Hysteresis)

    static Settings * instance()
    {
        static Settings m;
        return &m;
    }

    static QObject* qmlInstance(QQmlEngine *qmlEngine, QJSEngine* jsEngine)
    {
        Q_UNUSED(jsEngine)
        qmlEngine->setObjectOwnership(instance(), QQmlEngine::CppOwnership);
        return instance();
    }

    int targetTempDecDegC(const Profile profile) const;
    bool setTargetTempDecDegC(const int temp, const Profile profile);

    Hysteresis hysteresis(const Profile profile);
    void setHysteresis(const Hysteresis hyst, const Profile profile);

    static int minTempDecDegC() { return MIN_TEMP_DEC_DEG_C; }
    static int maxTempDecDegC() { return MAX_TEMP_DEC_DEG_C; }

    static int numberOfSlots() { return 4*24; }

    Profile profileInDaySlot(const Day day, const int slot);
    QVector<Profile> profilesInDay(const Day day);
    bool setProfileInDaySlot(const Day day, const int slot, const Profile profile);

    void setTurnedOn(bool status);
    bool turnedOn() const;

private:
    explicit Settings(QObject *parent = nullptr);
    ~Settings() = default;
    Settings* operator=(Settings& other) = delete;
    Settings(const Settings& other) = delete;

    /// Minimum temperature in 0.1 ºC
    static const int MIN_TEMP_DEC_DEG_C = 150;
    /// Maximum temperature in 0.1 ºC
    static const int MAX_TEMP_DEC_DEG_C = 250;

    QSettings * mSettings;
};
