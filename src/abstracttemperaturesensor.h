#ifndef ABSTRACTTEMPERATURESENSOR_H
#define ABSTRACTTEMPERATURESENSOR_H

#include <QObject>

class AbstractTemperatureSensor : public QObject
{
    Q_OBJECT
public:
    explicit AbstractTemperatureSensor(QObject *parent = nullptr);

    /**
     * @brief currentTemperatureDecDegC Virtual method that must be implemented
     * by classes inheriting this one.
     * @return The current temperature in 0.1 ºC.
     */
    virtual int currentTemperatureDecDegC() const = 0;

signals:
    void currentTemperatureChanged();
};

#endif // ABSTRACTTEMPERATURESENSOR_H
