#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTranslator>
#include <QLibraryInfo>

#include "settings.h"
#include "temperatureengine.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    // Basic app info.
    app.setOrganizationName("HeatControl");
    app.setOrganizationDomain("ar.com.perezmeyer");
    app.setApplicationName("heatcontrol");
    app.setApplicationVersion(HEATCONTROL_VERSION);

    // I18n
    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QTranslator qtBaseTranslator;
    qtBaseTranslator.load("qtbase_" + QLocale::system().name(),
                          QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtBaseTranslator);

    /*
     * Try loading the translations from the system's normal path first, else
     * at the same path as the binary itself.
     */
    QTranslator appTranslator;
    if(!appTranslator.load("/usr/share/heatercontrol/qm/heatercontrol_" + QLocale::system().name()))
        appTranslator.load("heatercontrol_" + QLocale::system().name());
    app.installTranslator(&appTranslator);

    qmlRegisterSingletonType<TemperatureEngine>("Helpers", 1, 0, "Settings", &Settings::qmlInstance);
    qmlRegisterSingletonType<TemperatureEngine>("Helpers", 1, 0, "TemperatureEngine", &TemperatureEngine::qmlInstance);

    QQmlApplicationEngine engine;

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
