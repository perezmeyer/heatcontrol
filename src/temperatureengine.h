#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QJSEngine>
#include <QTime>
#include <QString>

#include "settings.h"

class QTimer;
class AbstractTemperatureSensor;

class TemperatureEngine : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool turnedOn READ turnedOn WRITE setTurnedOn NOTIFY turnedOnChanged)
    Q_PROPERTY(bool heaterOn READ heaterOn WRITE setHeaterOn NOTIFY heaterOnChanged)
    Q_PROPERTY(QString currentTemp READ currentTemp NOTIFY currentTempChanged)
    Q_PROPERTY(QString currentDateTime READ currentDateTime NOTIFY currentDateTimeChanged)

public:

    enum OperationMode {
        Normal = 0,
        TimeOverride,
        TemperatureOverride
    };

    static TemperatureEngine * instance()
    {
        static TemperatureEngine m;
        return &m;
    }

    static QObject* qmlInstance(QQmlEngine *qmlEngine, QJSEngine* jsEngine)
    {
        Q_UNUSED(jsEngine)
        qmlEngine->setObjectOwnership(instance(), QQmlEngine::CppOwnership);
        return instance();
    }

    bool turnedOn() const { return mTurnedOn; }
    void setTurnedOn(bool status);

    bool heaterOn() const { return mHeatherOn; }
    void setHeaterOn(bool isOn);

    static int timeToSlot(const QTime & time);

    void setTemperatureSensor(AbstractTemperatureSensor * tempSensor);

    QString currentTemp();

    QString currentDateTime() const;

    OperationMode operationMode() const { return mOperMode; }
    void setTimeOverride(const int minutes, const int tempDecDegC, const Settings::Hysteresis hysteresys);
    void setTemperatureOverride(const Settings::Hysteresis tempDecDegC);
    void setNormalOperationMode();

signals:
    void turnedOnChanged();
    void heaterOnChanged();
    void currentTempChanged();
    void currentDateTimeChanged();
    void operationModeChanged();

private:
    explicit TemperatureEngine(QObject *parent = nullptr);
    ~TemperatureEngine() = default;
    TemperatureEngine* operator=(TemperatureEngine& other) = delete;
    TemperatureEngine(const TemperatureEngine& other) = delete;
    void evaluateStatus();
    void evaluateNormalOperationMode();
    void evaluateTimeOverrideOperationMode();
    void evaluateTemperatureOverrideOperationMode();

    bool mTurnedOn;
    bool mHeatherOn;
    OperationMode mOperMode;
    qint64 mEndTimeOverrideEpoch;
    int mOverrideTargetTemp;
    Settings::Hysteresis mOverrideHysteresis;

    QTimer * mTimer;
    AbstractTemperatureSensor * mTempSensor;

    friend class TemperatureEngineTests;
};
