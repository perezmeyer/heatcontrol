<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_AR">
<context>
    <name>HomeScreen</name>
    <message>
        <location filename="../Screens/HomeScreen.qml" line="87"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionButton</name>
    <message>
        <location filename="../Components/OptionButton.qml" line="21"/>
        <source>text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <location filename="../Components/SwitchButton.qml" line="9"/>
        <source>Off</source>
        <translation>Apagado</translation>
    </message>
    <message>
        <location filename="../Components/SwitchButton.qml" line="10"/>
        <source>On</source>
        <translation>Encendido</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="9"/>
        <source>Heat control</source>
        <translation>Control de calefacción</translation>
    </message>
</context>
</TS>
