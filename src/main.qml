import QtQuick 2.12
import QtQuick.Window 2.12
import "Screens"

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Heat control")

    Rectangle {
        id: mainContent
        anchors.fill: parent
        Loader {
            id: screenLoader
            anchors.fill: parent
            source: "Screens/HomeScreen.qml"
        }

        Connections {
            target: screenLoader.item
            ignoreUnknownSignals: true
        }
    }
}
