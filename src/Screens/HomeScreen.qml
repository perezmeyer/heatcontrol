import QtQuick 2.0
import Helpers 1.0
import "../Components"

Item {
    id: root
    anchors.fill: parent

    Rectangle {
        id: screen
        anchors.fill: parent
        color: "grey"
        property bool optionsShown: false

        DateTimeLabel {
            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
                margins: 6
            }
            height: parent.height / 10
            backgroundColor: parent.color
        }

        Image {
            source: "../images/noun_Fire_2898883.png"
            width: 100
            height: 100
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: 6
            visible: TemperatureEngine.heaterOn
        }

        Text {
            height: font.pixelSize
            anchors.centerIn: parent
            font.pixelSize: 80
            text: TemperatureEngine.currentTemp
        }

        SwitchButton {
            id: systemSwitch
            height: parent.height / 6
            width: height
            anchors {
                top: parent.top
                right: parent.right
            }

            color: parent.color

            checked: TemperatureEngine.turnedOn
            onCheckedChanged: TemperatureEngine.turnedOn = checked
        }

        MenuButton {
            id: menuButton
            backgroundColor: parent.color

            height: parent.height / 6
            width: height
            anchors {
                bottom: parent.bottom
                right: parent.right
            }

            onClicked: {
                screen.optionsShown = !screen.optionsShown
                manualButton.visible = screen.optionsShown
                settingsButton.visible = screen.optionsShown
            }
        }

        OptionButton {
            id: manualButton
            backgroundColor: parent.color
            anchors {
                top: menuButton.top
                bottom: menuButton.bottom
                right: menuButton.left
                rightMargin: 2
            }

            visible: false

            width: menuButton.width
            text: qsTr("M")
        }

        OptionButton {
            id: settingsButton
            backgroundColor: parent.color
            anchors {
                bottom: menuButton.top
                right: menuButton.left
                rightMargin: 2
            }

            height: manualButton.height

            visible: false

            width: menuButton.width
            text: qsTr("S")
        }
    }
}
