import QtQuick 2.0

Item {
    id: root
    signal clicked()
    signal released()

    property alias pressed: ma.pressed
    property color barsColor: "black"
    property color pressedBackgroundColor: "blue"
    property color backgroundColor: parent.color
    readonly property int barHeight: height / 7

    Rectangle {
        id: mainArea
        anchors.fill: parent

        color: backgroundColor

        Rectangle {
            anchors.horizontalCenter: mainArea.horizontalCenter
            anchors.top: mainArea.top
            anchors.topMargin: barHeight

            id: firstRect
            width: 0.8 * parent.width
            height: barHeight
            color: barsColor
        }

        Rectangle {
            anchors.horizontalCenter: mainArea.horizontalCenter
            anchors.top: mainArea.top
            anchors.topMargin: 3 * barHeight

            id: secondRect
            width: 0.8 * parent.width
            height: barHeight
            color: barsColor
        }

        Rectangle {
            anchors.horizontalCenter: mainArea.horizontalCenter
            anchors.top: mainArea.top
            anchors.topMargin: 5 * barHeight

            id: thirdRect
            width: 0.8 * parent.width
            height: barHeight
            color: barsColor
        }

        MouseArea {
            id: ma
            anchors.fill: parent
            onClicked: root.clicked()
            onPressed: mainArea.color = pressedBackgroundColor
            onReleased: {
                mainArea.color = root.backgroundColor
                root.released
            }
        }
    }
}
