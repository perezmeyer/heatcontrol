import QtQuick 2.0

Rectangle {
    id: root
    implicitHeight: 100
    implicitWidth: 200

    property bool checked: false
    property string offText: qsTr("Off")
    property string onText: qsTr("On")

    Rectangle {
        id: switchRect
        anchors.verticalCenter: root.verticalCenter
        height: root.height / 2
        width: root.width
        radius: width / 4
        color: Qt.lighter(root.color)

        Row {
            id: row
            anchors.fill: parent

            Rectangle {
                id: buttonCirc
                height: switchRect.height
                width: height
                radius: width / 2
                color: "black"
            }

            Text {
                id: stateText
                height: switchRect.height
                width: height
                text: offText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }


    }
    MouseArea {
        id: ma
        anchors.fill: parent
        onClicked: root.checked = root.checked ? false : true
    }

    onCheckedChanged: {
        row.layoutDirection = root.checked ? Qt.RightToLeft : Qt.LeftToRight
        stateText.text = root.checked ? onText : offText
    }
}
