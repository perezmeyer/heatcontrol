import QtQuick 2.0

Item {
    id: root
    signal clicked()

    implicitHeight: 10
    implicitWidth: 20

    property color backgroundColor: parent.color
    property alias text: buttonText.text

    Rectangle {
        id: button
        anchors.fill: parent
        color: "#00000000"

        Text {
            id: buttonText
            anchors.fill: parent
            text: qsTr("text")
            font.pixelSize: 5 * button.height / 7
            font.weight: Font.Bold
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        MouseArea {
            id: ma
            anchors.fill: parent
            onClicked: root.clicked
        }
    }
}
