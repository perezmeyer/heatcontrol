import QtQuick 2.12
import Helpers 1.0

Item {
    id: root
    property color textColor: "black"
    property alias backgroundColor: background.color
    implicitHeight: 16
    implicitWidth: txt_currentDate.paintedWidth
    Rectangle {
        id: background
        anchors.fill: parent
        Text {
            id: txt_currentDate
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 3
            }
            text: TemperatureEngine.currentDateTime
            color: textColor
            font.pixelSize: parent.height * 0.6
        }
    }
}
