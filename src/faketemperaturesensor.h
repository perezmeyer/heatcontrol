#ifndef FAKETEMPERATURESENSOR_H
#define FAKETEMPERATURESENSOR_H

#include <QObject>

#include "abstracttemperaturesensor.h"

class FakeTemperatureSensor : public AbstractTemperatureSensor
{
    Q_OBJECT
public:
    FakeTemperatureSensor(QObject * parent = nullptr);

    int currentTemperatureDecDegC() const { return mTemp;}

    void setCurrentTemperatureDecDegC(int temp);

private:
    int mTemp;
};

#endif // FAKETEMPERATURESENSOR_H
