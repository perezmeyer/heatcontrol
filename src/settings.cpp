#include <QSettings>
#include <QVector>
#include <QMetaEnum>

#include "settings.h"

/**
 * @brief Settings::targetTempDecDegC
 * @return The target temperature in 0.1 ºC
 */
int Settings::targetTempDecDegC(const Profile profile) const
{
    int temp;

    mSettings->beginGroup(QStringLiteral("degreesC"));
    temp = mSettings->value(QStringLiteral("targetTemp%1").arg(QMetaEnum::fromType<Profile>().valueToKey(profile)), MIN_TEMP_DEC_DEG_C).toInt();
    mSettings->endGroup();

    return temp;
}

/**
 * @brief Settings::setTargetTempDecDegC
 * @param temp Temperature in 0.1 ºC
 * @return True is the temperature could be set, false otherwise.
 */
bool Settings::setTargetTempDecDegC(const int temp, const Profile profile)
{
    if((temp < MIN_TEMP_DEC_DEG_C) || (temp > MAX_TEMP_DEC_DEG_C))
        return false;

    if(temp == targetTempDecDegC(profile))
        return true;

    mSettings->beginGroup(QStringLiteral("degreesC"));
    mSettings->setValue(QStringLiteral("targetTemp%1").arg(QMetaEnum::fromType<Profile>().valueToKey(profile)), temp);
    mSettings->endGroup();

    return true;
}

Settings::Hysteresis Settings::hysteresis(const Settings::Profile profile)
{
    Hysteresis hyst;

    mSettings->beginGroup(QStringLiteral("degreesC"));
    hyst = mSettings->value(QStringLiteral("hysteresis%1").arg(QMetaEnum::fromType<Profile>().valueToKey(profile)), TwoDec).value<Hysteresis>();
    mSettings->endGroup();

    return hyst;
}

void Settings::setHysteresis(const Hysteresis hyst, const Settings::Profile profile)
{
    if(hyst == hysteresis(profile))
        return;

    mSettings->beginGroup(QStringLiteral("degreesC"));
    mSettings->setValue(QStringLiteral("hysteresis%1").arg(QMetaEnum::fromType<Profile>().valueToKey(profile)), hyst);
    mSettings->endGroup();
}

Settings::Profile Settings::profileInDaySlot(const Settings::Day day, const int slot)
{
    if((slot < 0) || (slot >= numberOfSlots()))
        return InvalidProfile;

   return profilesInDay(day).at(slot);
}

QVector<Settings::Profile> Settings::profilesInDay(const Settings::Day day)
{
    QVector<Profile> dayProfiles;

    mSettings->beginReadArray(QMetaEnum::fromType<Day>().valueToKey(day));

    for (int i = 0; i < numberOfSlots(); ++i)
    {
        mSettings->setArrayIndex(i);
        dayProfiles.append(mSettings->value(QString("%1").arg(i), FirstProfile).value<Profile>());
    }

    mSettings->endArray();

    return dayProfiles;
}

bool Settings::setProfileInDaySlot(const Settings::Day day, const int slot, const Settings::Profile profile)
{
    if((slot < 0) || (slot >= numberOfSlots()))
        return false;

    mSettings->beginWriteArray(QMetaEnum::fromType<Day>().valueToKey(day));
    mSettings->setArrayIndex(slot);
    mSettings->setValue(QString("%1").arg(slot), profile);
    mSettings->endArray();

    return true;
}

void Settings::setTurnedOn(bool status)
{
    if(status == turnedOn())
        return;

    mSettings->beginGroup(QStringLiteral("General"));
    mSettings->setValue(QStringLiteral("turnedOn"), status);
    mSettings->endGroup();
}

bool Settings::turnedOn() const
{
    bool isOn;

    mSettings->beginGroup(QStringLiteral("General"));
    isOn = mSettings->value(QStringLiteral("turnedOn"), false).toBool();
    mSettings->endGroup();

    return isOn;
}

Settings::Settings(QObject *parent) :
    QObject(parent),
    mSettings(new QSettings(this))
{
}
