#include <QObject>
#include "abstracttemperaturesensor.h"
#include "settings.h"
#include "faketemperaturesensor.h"

FakeTemperatureSensor::FakeTemperatureSensor(QObject *parent) :
    AbstractTemperatureSensor(parent)
{
    mTemp = Settings::instance()->targetTempDecDegC(Settings::FirstProfile);
}

void FakeTemperatureSensor::setCurrentTemperatureDecDegC(int temp)
{
    mTemp = temp;

    Q_EMIT currentTemperatureChanged();
}
