#include <QObject>
#include <QTimer>
#include <QDateTime>

#include "settings.h"
#include "abstracttemperaturesensor.h"
#include "faketemperaturesensor.h"
#include "temperatureengine.h"

void TemperatureEngine::setTurnedOn(bool status)
{
    if(status == mTurnedOn)
        return;

    mTurnedOn = status;
    auto settings = Settings::instance();
    settings->setTurnedOn(mTurnedOn);

    // Evaluate the status at least each 30".
    if(mTurnedOn)
        mTimer->start(500);
    else
    {
        mTimer->stop();
        setHeaterOn(false);
    }

    Q_EMIT turnedOnChanged();
}

void TemperatureEngine::setHeaterOn(bool isOn)
{
    if(isOn == mHeatherOn)
        return;

    mHeatherOn = isOn;

    Q_EMIT heaterOnChanged();
}

int TemperatureEngine::timeToSlot(const QTime &time)
{
    // 24 hs * 60 mins / number of slots per day.
    static const int MIN_PER_SLOT = 24 * 60 / Settings::numberOfSlots();

    return (time.hour() * 60 + time.minute()) / MIN_PER_SLOT;
}

void TemperatureEngine::setTemperatureSensor(AbstractTemperatureSensor *tempSensor)
{
    if(tempSensor == nullptr)
        return;

    mTempSensor = tempSensor;
    evaluateStatus();
}

QString TemperatureEngine::currentTemp()
{
    return QStringLiteral("%1").arg(mTempSensor->currentTemperatureDecDegC()/10.0, 0, 'f', 1);
}

QString TemperatureEngine::currentDateTime() const
{
    auto dt = QDateTime::currentDateTime().toLocalTime();
    return dt.toString(Qt::TextDate);
}

/**
 * @brief TemperatureEngine::setTimeOverride Overrides the profile target
 * temperature for the specified time in minutes.
 * @param minutes
 * @param tempDecDegC
 */
void TemperatureEngine::setTimeOverride(const int minutes, const int tempDecDegC, Settings::Hysteresis hysteresis)
{
    if(minutes < 0)
        return;

    mOperMode = TimeOverride;
    mEndTimeOverrideEpoch = QDateTime::currentSecsSinceEpoch() + minutes * 60;
    mOverrideTargetTemp = tempDecDegC;
    mOverrideHysteresis = hysteresis;

    Q_EMIT operationModeChanged();
}

/**
 * @brief TemperatureEngine::setTemperatureOverride Turns on the heater until
 * the target temperature increases tempDecDegC. This is a one-shot behavior.
 * @param tempDecDegC
 */
void TemperatureEngine::setTemperatureOverride(const Settings::Hysteresis tempDecDegC)
{
    if(tempDecDegC < 0)
        return;

    mOperMode = TemperatureOverride;
    mOverrideTargetTemp = mTempSensor->currentTemperatureDecDegC() + static_cast<int>(tempDecDegC);

    Q_EMIT operationModeChanged();
}

void TemperatureEngine::setNormalOperationMode()
{
    mOperMode = Normal;
    evaluateNormalOperationMode();

    Q_EMIT operationModeChanged();
}

TemperatureEngine::TemperatureEngine(QObject *parent) :
    QObject(parent),
    mTurnedOn(false),
    mHeatherOn(false),
    mOperMode(Normal),
    mEndTimeOverrideEpoch(0),
    mOverrideTargetTemp(150),
    mTimer(new QTimer(this))
{
    mTempSensor = new FakeTemperatureSensor(this);

    auto settings = Settings::instance();
    setTurnedOn(settings->turnedOn());

    mTimer->setSingleShot(false);
    connect(mTimer, &QTimer::timeout, this, &TemperatureEngine::evaluateStatus);
}

void TemperatureEngine::evaluateStatus()
{
    Q_EMIT currentDateTimeChanged();

#ifndef HEATCONTROL_TEST
    if((QTime::currentTime().second() % 30) != 0)
        return;
#endif

    switch(mOperMode) {
    case Normal:
        evaluateNormalOperationMode();
        break;

    case TimeOverride:
        evaluateTimeOverrideOperationMode();
        break;

    case TemperatureOverride:
        evaluateTemperatureOverrideOperationMode();
        break;
    }
}

void TemperatureEngine::evaluateNormalOperationMode()
{
    const auto dateTime = QDateTime::currentDateTime().toLocalTime();
    const auto slot = timeToSlot(dateTime.time());
    const auto profile = Settings::instance()->profileInDaySlot(static_cast<Settings::Day>(dateTime.date().dayOfWeek()), slot);
    const auto targetTemp = Settings::instance()->targetTempDecDegC(profile);
    const auto hyst = static_cast<int>(Settings::instance()->hysteresis(profile));

    auto currentTemp = mTempSensor->currentTemperatureDecDegC();
    Q_EMIT currentTempChanged();

    if(currentTemp <= targetTemp - hyst)
        setHeaterOn(true);
    else if(currentTemp >= targetTemp + hyst)
        setHeaterOn(false);
}

void TemperatureEngine::evaluateTimeOverrideOperationMode()
{
    auto epoch = QDateTime::currentSecsSinceEpoch();
    if(epoch >= mEndTimeOverrideEpoch)
    {
        mOperMode = Normal;
        evaluateNormalOperationMode();
        Q_EMIT operationModeChanged();

        return;
    }

    auto currentTemp = mTempSensor->currentTemperatureDecDegC();
    Q_EMIT currentTempChanged();

    const auto hyst = static_cast<int>(mOverrideHysteresis);

    if(currentTemp <= mOverrideTargetTemp - hyst)
        setHeaterOn(true);
    else if(currentTemp >= mOverrideTargetTemp + hyst)
        setHeaterOn(false);
}

void TemperatureEngine::evaluateTemperatureOverrideOperationMode()
{
    auto currentTemp = mTempSensor->currentTemperatureDecDegC();
    Q_EMIT currentTempChanged();

    if(currentTemp >= mOverrideTargetTemp)
    {
        mOperMode = Normal;
        evaluateNormalOperationMode();
        Q_EMIT operationModeChanged();

        return;
    }
}
